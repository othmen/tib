<?php

/**
* @file
* Grab the latest stock info from Yahoo
*/

// 3600 is once an hour
define('STOCKAPI_CRON_UPDATE_FREQUENCY', 3600);
//Update 30 stock quotes per hour.
define('STOCKAPI_CRON_ROWS_PER_FREQ',    30);

/********************************************************************
* Drupal Hooks
********************************************************************/

/**
 * Implementation of hook_menu().
 */
function stockapi_menu() {
  $items = array();

  $items['admin/settings/stockapi'] = array(
    'title' => 'Stock API settings',
    'description' => t('Configure cron run settings.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('stockapi_admin_settings'),
    'access arguments' => array('administer site configuration'),
  );

  return $items;
}

function stockapi_admin_settings() {
  $period = drupal_map_assoc(array(900, 1800, 3600, 10800, 21600, 32400, 43200, 86400), 'format_interval');
  $rows   = drupal_map_assoc(array(30, 100, 500, 1000, 2500, 5000, 10000));

  $form['stockapi_fetch'] = array(
    '#type' => 'select',
    '#title' => t('Stock data update frequency'),
    '#default_value' => variable_get('stockapi_fetch', STOCKAPI_CRON_UPDATE_FREQUENCY),
    '#options' => $period,
    '#description' => t('How often to refresh the stock data from Yahoo! Finance. Default is 1 hour (3600 seconds).'),
  );

  $form['stockapi_rows'] = array(
    '#type' => 'select',
    '#title' => t('Symbols to update each cron run cycle'),
    '#default_value' => variable_get('stockapi_rows', STOCKAPI_CRON_ROWS_PER_FREQ),
    '#options' => $rows,
    '#description' => t('How many symbols to update every cron run. This limit is imposed so we do not overload the host.'),
  );

  return system_settings_form($form);
}

/**
 * Implementation of hook_cron().
 */
function stockapi_cron() {
  if (time() - variable_get('stockapi_fetch_last', 0) > variable_get('stockapi_fetch', STOCKAPI_CRON_UPDATE_FREQUENCY)) {
    $result = db_query_range('SELECT symbol FROM {stockapi} ORDER BY updated', 0, variable_get('stockapi_rows', STOCKAPI_CRON_ROWS_PER_FREQ));
    while ($data = db_fetch_object($result)) {
      if ($stock = stockapi_fetch($data->symbol)) {
        stockapi_save($stock);
      }
    }
    variable_set('stockapi_fetch_last', time());
    cache_clear_all('variables', 'cache');
    watchdog('cron', t('Stock API updated.'));
  }
}

/**
 * Fetch the stock info for a given ticker symbol from Yahoo!
 */
function stockapi_fetch($symbol) {
  if (empty($symbol)) {
    return FALSE;
  }

  $fields = 'snl1c1ohgvd1t1';
  /*
    "s"  => "Symbol",
    "n"  => "Name",
    "l1" => "Last",
    "c1" => "Change",
    "o"  => "Opening",
    "h"  => "High",
    "g"  => "Low",
    "v"  => "Volume",
    "d1" => "Date",
    "t1" => "Time"
   */

  $host = 'http://download.finance.yahoo.com';
  $url = $host .'/d/quotes.csv?s='. urlencode($symbol) .'&f='.$fields.'&e=.csv';

  $result = drupal_http_request($url);
  if (isset($result->error)) {
    watchdog('stockapi', t('StockAPI: drupal_http_request error: @error', array('@error' => $result->error)));
    return FALSE;
  }

  if ($result->code != 200) {
    watchdog('stockapi', t('StockAPI: drupal_http_request code: @code', array('@code' => $result->code)));
    return FALSE;
  }

  $data = explode(',', $result->data);

  for ($i=0; $i < count($data); $i++) {
    $value = trim($data[$i]);
    if ((substr($value, 0, 1) == '"') && (substr($value, strlen($value) - 1, 1)) != '"') {
      $data[$i] = str_replace('"', '', ($value .','. $data[$i+1]));
      unset($data[$i+1]);
      $i++;
    }
    else {
      $data[$i] = str_replace('"', '', $value);
    }
  }

  if (!$data) {
    watchdog('stockapi', t('StockAPI: no data for symbol(s): @symbol', array('@symbol' => $symbol)));
    return FALSE;
  }
  
  $data = array_values($data);
  return $data;
}

function stockapi_load($symbol) {
  if (!empty($symbol)) {
    $stock = db_fetch_array(db_query("SELECT * FROM {stockapi} WHERE symbol = '%s'", $symbol));
    if (!$stock) {
      $stock = stockapi_fetch($symbol);
      if ($stock) {
        if ($stock[8] != 'N/A') {
          // Date is 'N/A' means an invalid stock symbol
          stockapi_save($stock);

          // Load again so it can show for the first time
          $stock = stockapi_load($symbol);
        }
      }
    }
  }

  return $stock;
}

function stockapi_save($stock) {
  $symbol = $stock[0];
  if (!empty($symbol)) {
    stockapi_delete($symbol);
    stockapi_insert($stock);
  }
}

function stockapi_insert($stock) {
  return db_query("INSERT INTO {stockapi}
    VALUES ('%s', '%s', %f, %f, %f, %f, %f, %d, '%s', '%s', %d)",
    $stock[0],
    $stock[1],
    $stock[2],
    $stock[3],
    $stock[4],
    $stock[5],
    $stock[6],
    $stock[7],
    $stock[8],
    $stock[9],
    time());
}

function stockapi_delete($symbol) {
  return db_query("DELETE FROM {stockapi} WHERE symbol = '%s'", $symbol);
}
