
Description:
------------
        
This is a stock quote api module that provides other modules with a facility 
to incorporate stock data in them.

Features:
---------

This module provides several features and options:

* When a stock is first queried, the request goes over the internet to
  Yahoo Finance. The data retrieved is then stored in a database
  table for later fetches by modules.

* The stock data in the table are periodically refreshed from Yahoo,
  at an admin specified interval.

* The number of rows to update every cron run is also configurable.

Demos:
------

http://buyblue.org uses this module for stock quotes

http://2bits.com/stock has the stock module installed, which uses
this stockapi module.

Database:
---------

This module requires that the database table 'stockapi' be created.

Installation:
-------------

- Cron must be configured correctly and running on your site
- Copy the stockapi module directory to the modules directory
- Enable the module in Administer > Modules
- You can change the update frequency in Administer > Settings > StockAPI

Bugs/Features/Patches:
----------------------

If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.
http://drupal.org/project/stockapi

Author
------

Originally developed by Matt Westgate http://asitis.org for BuyBlue.com

Modified and maintained by Khalid Baheyeldin http://baheyeldin.com/khalid
and http://2bits.com

The author(s) can also be contacted for paid customizations of this
and other modules.
