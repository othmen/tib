Drupal 6.x, by vanzoo

6.x-1.0: CHG: Removed Listpage for news
	 NEW: option to change delay time
	 NEW: option to enable or disable fading
	 NEW: option per ticker to enable or disable linking to node
	 NEW: port to drupal 6.x
	 BUG: Fix to nodelink using basepath

Drupal 5.x:

5.x-1.0: First version of Newsflash
5.x-1.1: Bug fix: using Drupal "files" folder for Windows and Linux compatible
5.x-1.2: Bug fix: Set "promote to frontpage=FALSE"
5.x-1.3: New: use cron to update AjAX Newsflash
         Bug fix: Add error message to watchdog instead die the page while
         this modoule can't create tickercontent.txt file
5.x-1.4: Bug fix: Intialiazing ticker when Drupal installed on sub-folder
5.x-1.5: Bug fix: Check if base_path() has no value
5.x-1.6: New: Use "teaser" for block and "body" for entire content
5.x-1.7: New: Support 3 Newsflash blocks
5.x-1.8: Bug fix: Fix codes in install.php
         New: no need cron anymore! Now, automatic update after Submit new Newflash!
5.x-1.9: Bug fix: check if table content_type_newsflash exists before
5.X-1.10: check if content_type_newsflash and content_type_newsflash1 exists
5.x-1.11: Bug fix: rename ajax_newflash.css to ajax_newsflash.css 
          thanks to greekman (http://drupal.org/node/282443)
5.x-1.12: Bug fix: Uninstall drops content_type_newsflash
5.x-1.13: Bug fix: Update file on Insert and Update action
5.x-1.14: Bug fix: In .info file, name = "Ajax Newsflash" instead ""Ajax Newsflash"" 
5.x-1.15: New: add setting page
          New: add 'more' link on the block and set how many items per page
          New: show newsflash items on table format, you can set how many columns