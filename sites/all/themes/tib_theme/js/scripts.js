$(document).ready(function() {

	$('ul.primary-links li').hover(
		function () {
			$(this).addClass('over');
	  	}, 
	  	function () {
	  	  $(this).removeClass('over');
	  }
	);
	
	$('#search-block-form input.form-text').searchEvent();
	$('.lsRight').click(function(){
		if($(".lsTarget:visible").length == 0){
			if ($.browser.msie){
				$(".lsTarget").css('display','block');
			}else{
				$('.lsTarget').slideDown("fast");
			}
		}else{
			if ($.browser.msie){
				$(".lsTarget").css('display','none');
			}else{
				$('.lsTarget').slideUp("fast");
			}			
		}
	});
	
	$("#block-views-banners-block_1 .view-content div.item-list ul").cycle({fx: 'fade'});

	Cufon.replace('body:not(.i18n-ar) h1, body:not(.i18n-ar) h2, body:not(.i18n-ar) h3, body:not(.i18n-ar) h4', { fontFamily: 'Delicious' });
	Cufon.replace('body:not(.i18n-ar) button', { fontFamily: 'Delicious', fontSize: '16px' });
	Cufon.replace('body:not(.i18n-ar) span.pmNormal, body:not(.i18n-ar) span.pmHover', { fontFamily: 'Delicious', fontSize: '16px' });

	//Cufon.replace('body.i18n-ar h1, body.i18n-ar h2', { fontFamily: 'Andalus' });
	//Cufon.replace('body:.i18n-ar span.pmNormal, body:not(.i18n-ar) span.pmHover', { fontFamily: 'Delicious' });
});

$.fn.searchEvent = function () {
	var txtSearch = Drupal.t('Search...');
	if(this.val() == ''){
		this.val(txtSearch);
	}else{
		txtSearch = this.text();
	}
	
	this.focus(function(){
		if($(this).val() == txtSearch){
			$(this).val('');
		}
	}).blur(function() {
		if($(this).val() == ''){
			$(this).val(txtSearch);
		}
	});
}