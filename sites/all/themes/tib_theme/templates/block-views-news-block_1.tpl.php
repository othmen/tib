<?php
	global $language;
	$path = drupal_get_path('theme', 'tib_theme');
	$image = $path . '/images/last-news_' . $language->language . '.gif';
    $flux = views_get_view_result('aggregator');
    $results = views_get_view_result('news', 'block');
    //dpm($results);
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>">

	<?php print l(t('All News'), 'news', array('attributes'=>array('class'=>'all-items'))); ?>
	<?php if (!empty($block->subject)): ?>
  	<h2 class="title"><?php print $block->subject ?></h2>
	<?php endif;?>

  <div class="content"><?php print $block->content ?></div>
</div>
