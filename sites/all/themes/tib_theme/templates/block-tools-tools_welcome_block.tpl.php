<?php
	global $language;
	$image = tools_full_path(variable_get('tools_welcome_block_image_'.drupal_strtolower($language->language), WELCOME_IMAGE_.drupal_strtoupper($language->language)));
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>">

	<?php if (!empty($block->subject)): ?>
  	<h2 class="title"><img src="<?php print $image; ?>" alt="<?php print $block->subject ?>" title="<?php print $block->subject ?>" /></h2>
	<?php endif;?>

  <div class="content"><?php print $block->content ?></div>
</div>
