<?php
$created = $fields['field_news_date_value']->content;
$body = $fields['body']->content;
$nid = $fields['nid']->content;
?>

<a href="<?php print url('node/'.$nid)?>" class="news-item">
	<span class="news-date"><?php print $created; ?></span>
	<span class="news-body"><?php print $body ?></span>
</a>