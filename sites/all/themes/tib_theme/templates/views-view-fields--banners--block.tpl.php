<?php
$image = $fields['field_banner_fid']->content;
$title = $fields['title']->content;
$body = $fields['teaser']->content;
$link = $fields['view_node']->content;
?>

<div id="banner-content">
	<?php print $image; ?>
	<div class="banner-left">
		<p><?php print $body; ?></p>
		<?php print $link; ?>
	</div>
</div>