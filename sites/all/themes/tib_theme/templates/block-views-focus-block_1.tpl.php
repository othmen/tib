<?php
	global $language;
	$path = drupal_get_path('theme', 'tib_theme');
	$image = $path . '/images/focus-on_' . $language->language . '.gif';
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>">

	<?php print l(t('All Focus'), 'focus', array('attributes'=>array('class'=>'all-items'))); ?>
	<?php if (!empty($block->subject)): ?>
  	<h2 class="title"><?php print $block->subject ?></h2>
	<?php endif;?>

  <div class="content"><?php print $block->content ?></div>
</div>
