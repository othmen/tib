<?php
$image = $fields['field_news_image_fid']->content;
$created = $fields['field_news_date_value']->content;
$title = $fields['title']->content;
$nid = $fields['nid']->content;
?>

<a href="<?php print url('node/'.$nid)?>" class="news-ontop">
	<?php print $image ?>
	<span class="news-infos">
		<span class="news-title"><?php print $title ?></span>
		<span class="news-date"><?php print $created; ?></span>
	</span>
</a>