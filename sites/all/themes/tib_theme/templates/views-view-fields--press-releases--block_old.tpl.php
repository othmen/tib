<?php
$date = $fields['field_date_press_value']->content;
$title = $fields['title']->content;
$body = $fields['body']->content;
$icon = tools_filefield_icon($fields['filemime']->content);
$mime = $fields['filemime']->content;
$size = $fields['filesize']->content;
$path = $fields['filepath']->content;
?>

<a href="<?php print file_create_url($path)?>" class="file-download">
	<span class="date-display-single"><?php print $title; ?></span>
	<span class="file-title"><?php print $body ?></span>
	<span class="file-infos">
		<?php print $icon ?>
		<?php print $size ?>
	</span>
</a>