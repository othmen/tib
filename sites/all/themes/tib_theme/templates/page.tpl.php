<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lt IE 7]>
      <?php print phptemplate_get_ie_styles(); ?>
    <![endif]-->
  </head>
  <body class="<?php print $body_classes; ?>">

<!-- Layout -->

    <div id="wrapper">
      <div id="header">
      	<div id="header_wrapper">
	        <div id="logo-floater">
	        <?php
	          // Prepare header
	          $site_fields = array();
	          if ($site_name) {
	            $site_fields[] = check_plain($site_name);
	          }
	          if ($site_slogan) {
	            $site_fields[] = check_plain($site_slogan);
	          }
	          $site_title = implode(' ', $site_fields);
	          if ($site_fields) {
	            $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
	          }
	          $site_html = implode(' ', $site_fields);
	
	          if ($logo || $site_title) {
	            print '<h1><a href="'. check_url($front_page) .'" title="'. $site_title .'">';
	            if ($logo) {
	              print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
	            }
	            print $site_html .'</a></h1>';
	          }
	        ?>
	        </div>
	        
	        <?php if ($header) : ?>
  	    		<div id="header_region" class="clear-block">
    	     		<?php print $header ?>
						</div>
        	<?php endif; ?>
  			</div>
	        
	      <?php if ($header_menu || $primary_links || $secondary_links) : ?>
  	    	<div id="header_menu" class="clear-block">
  	    		<div id="header_menu_wrapper">
        			<?php if ($header_menu) : ?>
        				<?php print $header_menu ?>
        			<?php endif; ?>
        			<?php if ($primary_links) : ?>
        				<?php print primary_links($primary_links, array('class' => 'links primary-links')) ?>
        			<?php endif; ?>
        			<?php if ($secondary_links) : ?>
          			<?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
        			<?php endif; ?>
    	   		</div>
					</div>
        <?php endif; ?>
        
        <?php if (isset($header_slide)) : ?>
          <div id="header_slide_wrapper" >
         		<?php print $header_slide ?>
					</div>
        <?php endif; ?>
      </div> <!-- /header -->
      <div class="clearboth"></div>
      <div id="header_slide_wrapper">
          <?php print $news_bar ?>
          <div class="clearboth"></div>
      </div>
      <div class="clearboth"></div>
      
			<!-- Content -->
			<div id="content">
	      <?php if ($top): ?>
  	      <div id="bar_top" class="bar">
    	      <?php print $top ?>
        	</div>
 	      	<div class="clearboth"></div>
	      <?php endif; ?>
				
	      <?php if ($left): ?>
  	      <div id="sidebar_left" class="sidebar">
    	      <?php if ($search_box): ?><div class="block block-theme">
                  <?php print $search_box ?></div><?php endif; ?>
      	    <?php print $left ?>
        	</div>
	      <?php endif; ?>
        
      
           <?php if ($center_content): ?>
            <div class="clearboth"></div>
            <div id="center_content">
                <?php print $center_content ?>
                <div class="clearboth"></div>
            </div>
            <?php endif; ?>
	      <?php if ($right): ?>
  	      <div id="sidebar_right" class="sidebar">
    	      <?php if (!$left && $search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>
      	    <?php print $right ?>
        	</div>
	      <?php endif; ?>

	      <div id="center_wrapper">
		      <?php if ($content_top): ?>
  		      <div id="content_top" class="content-block">
      		    <?php print $content_top ?>
        		</div>
	      	<?php endif; ?>
	      	
	      	<div id="center">
  	        <?php print $breadcrumb; ?>
    	      <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
      	    <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
        	  <?php if ($title): print '<h1 class="title'. ($tabs ? ' with-tabs' : '') .'">'. $title .'</h1>'; endif; ?>
          	<?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
	          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
  	        <?php if ($show_messages && $messages): print $messages; endif; ?>
    	      <?php print $help; ?>
      	    <div class="clear-block">
              <?php if(!drupal_is_front_page()):?>
        	    	<?php print $content ?>
              <?php endif; ?>
          	</div>
	          <?php //print $feed_icons ?>
         	</div>
	          
		      <?php if ($content_bottom): ?>
  		      <div id="content_bottom" class="content-block">
      		    <?php print $content_bottom ?>
        		</div>
	      	<?php endif; ?>
  	    </div>
  	    
	      <?php if ($bottom): ?>
  	    	<div class="clearboth"></div>
  	      <div id="bar_bottom" class="bar">
    	      <?php print $bottom ?>
        	</div>
	      <?php endif; ?>
			</div> <!-- /Content -->
			
      <div class="clearboth"></div>
      
      <div id="footer">
      	<div id="footer_wrapper">
      		<?php if ($footer): ?>
      			<div id="footer_left">
      				<?php print $footer ?>
      			</div>
	      	<?php endif; ?>
      		
      		<div id="footer_right">
      			<div class="footer_message">
          	<?php print t($footer_message) ?>
      			<?php print $footer_right ?>
      			</div>
      			<?php print l(theme('image', drupal_get_path('theme','tib_theme').'/images/logo_equinoxes.png'), 'http://www.equinoxes.net/', array('html'=>TRUE, 'attributes' => array('class' => 'equinoxes'))); ?>
      		</div>
      	</div>
      </div>

  	</div>
		<!-- /layout -->

  <?php print $closure ?>
  </body>
</html>
