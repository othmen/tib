<?php
	global $language;
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>"<?php print $block->style ?>>
  <div class="content">
	  <div class="language-select">
	    <img src="<?php print drupal_get_path('theme','tib_theme'); ?>/images/blank.gif" class="lsLeft">
	    <div class="lsRight"><?php print $language->language; ?></div>
	    <div class="lsTarget" style="display: none;">
	    	<?php print $block->content ?>
	    </div>
	  </div>
   </div>
</div>
