<?php
	global $language;
	$path = drupal_get_path('theme', 'tib_theme');
	$image = $path . '/images/press-releases_' . $language->language . '.png';
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>">

	<?php if (!empty($block->subject)): ?>
  	<h2 class="title"><?php print $block->subject ?></h2>
	<?php endif;?>

  <div class="content"><?php print $block->content ?></div>
</div>
