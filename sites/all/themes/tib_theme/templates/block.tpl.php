<?php
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?>">

<?php if (!empty($block->subject)): ?>
  <h2 class="title"><?php print t($block->subject) ?></h2>
<?php endif;?>

  <div class="content"><?php print $block->content ?></div>
</div>
