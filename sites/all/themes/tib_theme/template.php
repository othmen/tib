﻿<?php 

/**
 * Sets the body-tag class attribute.
 *
 * Adds 'sidebar-left', 'sidebar-right' or 'sidebars' classes as needed.
 */
function phptemplate_body_class($left, $right) {
  if ($left != '' && $right != '') {
    $class = 'sidebars';
  }
  else {
    if ($left != '') {
      $class = 'sidebar-left';
    }
    if ($right != '') {
      $class = 'sidebar-right';
    }
  }

  if (isset($class)) {
    print ' class="'. $class .'"';
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  /*if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' › ', $breadcrumb) .'</div>';
  }*/
	return '';
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function phptemplate_preprocess_page(&$vars) {
  $vars['tabs2'] = menu_secondary_local_tasks();
  $vars['primary_links'] = tib_menu_primary_links(variable_get('menu_primary_links_source', 'primary-links'));
}

/**
 * Add a "Comments" heading above comments except on forum pages.
 */
function garland_preprocess_comment_wrapper(&$vars) {
  if ($vars['content'] && $vars['node']->type != 'forum') {
    $vars['content'] = '<h2 class="comments">'. t('Comments') .'</h2>'.  $vars['content'];
  }
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  return menu_primary_local_tasks();
}

/**
 * Returns the themed submitted-by string for the comment.
 */
function phptemplate_comment_submitted($comment) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp)
    ));
}

/**
 * Returns the themed submitted-by string for the node.
 */
function phptemplate_node_submitted($node) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
    ));
}

/**
 * Generates IE CSS links for LTR and RTL languages.
 */
function phptemplate_get_ie_styles() {
  global $language;

  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/fix-ie.css" />';
  if ($language->direction == LANGUAGE_RTL) {
    $iecss .= '<style type="text/css" media="all">@import "'. base_path() . path_to_theme() .'/fix-ie-rtl.css";</style>';
  }

  return $iecss;
}

function primary_links($links, $attributes = array('class' => 'links')) {
  global $language;
  $output = '';

  if (count($links) > 0) {
    $output = '<ul' . drupal_attributes($attributes) . '>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;
      $link['html'] = true;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
           && (empty($link['language']) || $link['language']->language == $language->language)) {
        $class .= ' active';
      }
      $output .= '<li id="menuNav_' . $i . '"' . drupal_attributes(array('class' => $class)) . '>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $normal = '<span class="pmNormal">'.$link['title'].'</span>';
        $hover = '<span class="pmHover">'.$link['title'].'</span>';
        $descr = '<span class="pmDescription">'.$link['attributes']['title'].'</span>';
        $output .= l('<span class="pmRight">'.$normal.$hover.$descr.'</span>', $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
      }
      
      if (isset($link['below']) && !empty($link['below'])) {
        $output .= '<ul class="subMenu">';

		    $num_sublinks = count($link['below']);
		    $j = 1;

  	  	foreach ($link['below'] as $key => $link) {
  		    $class = $key;
    		  $link['html'] = true;

		      // Add first, last and active classes to the list of links to help out themers.
		      if ($j == 1) {
		        $class .= ' first';
		      }
		      if ($j == $num_sublinks) {
		        $class .= ' last';
		      }
		      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
		           && (empty($link['language']) || $link['language']->language == $language->language)) {
		        $class .= ' active';
		      }
		      $output .= '<li id="menuSubNav_' . $i . '-' . $j . '"' . drupal_attributes(array('class' => $class)) . '>';
			
		      if (isset($link['href'])) {
  		      // Pass in $link as $options, they share the same keys.
      		  $output .= l($link['title'], $link['href'], $link);
		      }
    		  else if (!empty($link['title'])) {
        		// Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        		if (empty($link['html'])) {
        		  $link['title'] = check_plain($link['title']);
        		}
        		$span_attributes = '';
        		if (isset($link['attributes'])) {
        		  $span_attributes = drupal_attributes($link['attributes']);
        		}
        		$output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
      		}
	
  		    $j++;
    		  $output .= "</li>\n";
    		}

    		$output .= '</ul>';
  		}

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}

function tib_menu_primary_links($menu_name, $level = 0) {
	global $language;
  // Don't even bother querying the menu table if no menu is specified.
  if (empty($menu_name)) {
    return array();
  }

  // Get the menu hierarchy for the current page.
  $tree = menu_tree_page_data($menu_name);

  // Go down the active trail until the right level is reached.
  while ($level-- > 0 && $tree) {
    // Loop through the current level's items until we find one that is in trail.
    while ($item = array_shift($tree)) {
      if ($item['link']['in_active_trail']) {
        // If the item is in the active trail, we continue in the subtree.
        $tree = empty($item['below']) ? array() : $item['below'];
        break;
      }
    }
  }

  // Create a single level of links.
  $links = array();
  foreach ($tree as $item) {
    if (!$item['link']['hidden'] && ($item['link']['localized_options']['langcode'] == $language->language || !$item['link']['localized_options']['langcode'])) {
      $class = '';
      $l = $item['link']['localized_options'];
      $l['href'] = $item['link']['href'];
      $l['title'] = $item['link']['title'];
      if ($item['link']['in_active_trail']) {
        $class = ' active-trail';
      }
      if(!empty($item['below'])){
      	$below = array();
      	
      	foreach($item['below'] as $sub){
      		if (!$sub['link']['hidden']) {
      			$subclass = '';
      			$b = $sub['link']['localized_options'];
      			$b['href'] = $sub['link']['href'];
      			$b['title'] = $sub['link']['title'];
      			if ($sub['link']['in_active_trail']) {
      				$subclass = ' active-trail';
      			}
      			
      			// Keyed with the unique mlid to generate classes in theme_links().
      			$below['menu-' . $sub['link']['mlid'] . $subclass] = $b;
      		}
      	}
      	$l['below'] = $below;
      }
      
      // Keyed with the unique mlid to generate classes in theme_links().
      $links['menu-' . $item['link']['mlid'] . $class] = $l;
    }
  }
  return $links;
}

function tib_theme_button($element) {
  // Make sure not to overwrite classes.
  if (isset($element['#attributes']['class'])) {
    $element['#attributes']['class'] = 'form-' . $element['#button_type'] . ' ' . $element['#attributes']['class'];
  }
  else {
    $element['#attributes']['class'] = 'form-' . $element['#button_type'];
  }
  
  $button = '<button type="submit" ' . (empty($element['#name']) ? '' : 'name="' . $element['#name'] . '" ');
  $button .= 'id="' . $element['#id'] . '" ';
  $button .= 'value="' . check_plain($element['#value']) . '" ';
  $button .= drupal_attributes($element['#attributes']);
  $button .= '>';
  $button .= '<span><span><span>';
  $button .= check_plain($element['#value']);
  $button .= '</span></span></span>';
  $button .= "</button>\n";

  return $button;
}